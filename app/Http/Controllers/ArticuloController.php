<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articulo;


class ArticuloController extends Controller
{
    public function index()
    {
        $articulo = Articulo::orderBy('created_at', 'DESC')->get();
        
        return $articulo;
    }

    public function obtenerArticulo($id)
    {
        $articulo = Articulo::find($id);
        return $articulo;
    }
}
