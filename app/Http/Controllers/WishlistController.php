<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wishlist;
use Illuminate\Support\Facades\DB;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Mail;
use App\Mail\AddProductToWislist;
use App\Mail\BuyProduct;
use App\Models\Articulo;
use App\Models\User;
use Carbon\Carbon;




class WishlistController extends Controller
{
    use ApiResponser;
    public function index()
    {
         $data = DB::table('wishlists')
         ->join('articulos','wishlists.articulo_id','=','articulos.id')
         ->join('users','wishlists.user_id','=','users.id')
         ->select('wishlists.id as id','wishlists.user_id as user_id', 'wishlists.articulo_id as articulo_id', 'wishlists.comprado as status', 'articulos.nombre as nombre','articulos.foto as foto',
                  'articulos.descripcion as descripcion','articulos.precio as precio', 'users.name as username', 'users.email as correo'
                  ,DB::raw('DATE(wishlists.updated_at) as fecha'))
         ->orderBy('wishlists.id','desc')
         ->get();
         if($data == false){
             return $this->errorResponse('No se puede visualizar', 404);  
         }else{
             return $this->successResponse($data);
         }

    }
    public function comprado()
    {
         $data = DB::table('wishlists')
         ->join('articulos','wishlists.articulo_id','=','articulos.id')
         ->join('users','wishlists.user_id','=','users.id')
         ->select('wishlists.id as id','wishlists.user_id as user_id', 'wishlists.articulo_id as articulo_id', 'wishlists.comprado as status', 'articulos.nombre as nombre','articulos.foto as foto',
                  'articulos.descripcion as descripcion','articulos.precio as precio', 'users.name as username', 'users.email as correo'
                  ,DB::raw('DATE(wishlists.updated_at) as fecha'))
         ->where('wishlists.comprado', '=', 1)
         ->orderBy('wishlists.id','desc')
         ->get();
         if($data == false){
             return $this->errorResponse('No se puede visualizar ', 404);  
         }else{
             return $this->successResponse($data);
         }

    }

    public function Nocomprado()
    {
         $data = DB::table('wishlists')
         ->join('articulos','wishlists.articulo_id','=','articulos.id')
         ->join('users','wishlists.user_id','=','users.id')
         ->select('wishlists.id as id','wishlists.user_id as user_id', 'wishlists.articulo_id as articulo_id', 'wishlists.comprado as status', 'articulos.nombre as nombre','articulos.foto as foto',
                  'articulos.descripcion as descripcion','articulos.precio as precio', 'users.name as username', 'users.email as correo'
                  ,DB::raw('DATE(wishlists.updated_at) as fecha'))
         ->where('wishlists.comprado', '=', 0)
         ->orderBy('wishlists.id','desc')
         ->get();
         if($data == false){
             return $this->errorResponse('No se puede visualizar ', 404);  
         }else{
             return $this->successResponse($data);
         }

    }

    public function guardar(Request $request)
    {
       $user_id = 1;
       $status = Wishlist::where('user_id',$user_id)
            ->where('articulo_id',$request->articulo_id)
            ->first();

        if(isset($status->user_id) && isset($request->articulo_id))
            {

                return $this->errorResponse('Articulo existente en Wishlist', 404);  
            }
            else{
               
                $wishlist = new Wishlist;
                $wishlist->user_id = $user_id ;
                $wishlist->user_id = $user_id ;
                $wishlist->articulo_id = $request->articulo_id;
                $wishlist->save();
        
                $articulo = Articulo::findOrFail($request->articulo_id);
                $user = User::findOrFail($user_id);
        
                Mail::to($user->email)->send(new AddProductToWislist($articulo));
            }
      
    }
   
    public function precioTotal()
    {  
        $user_id = 1;

        $data = DB::table('wishlists')
                   ->join('articulos','wishlists.articulo_id','=','articulos.id')
                   ->join('users','wishlists.user_id','=','users.id')
                   ->where('wishlists.comprado', 1 )
                   ->where('wishlists.user_id', 1 )
                   ->sum('articulos.precio' );
             return $data;
     

    }

  
    public function cambiarPresupuesto(Request $request)
    {   
        $user_id = 1;
        $user = User::where('id', $user_id)
        ->update(['presupuesto' => $request->presupuesto]);
        return $this->successResponse(
            'Cambiado Correctamente',
            202
        );
    }

    public function showPresupuesto()
    {   $user_id = 1;
        $user = User::select('presupuesto')->where('id', $user_id)->get();
        return $user;

    }
    
    public function montoDisponible(){
        $totalGastado = self::precioTotal();
        $presupuesto = self::showPresupuesto()[0]->presupuesto;
        $monto = $presupuesto - $totalGastado;
        return $monto;
    }


    public function update($id)
    {
        $monto = self::montoDisponible();
        $presupuesto = self::showPresupuesto()[0]->presupuesto;
        
        $articulo = DB::table('wishlists')
        ->join('articulos','wishlists.articulo_id','=','articulos.id')
        ->select('articulos.nombre as nombre','articulos.precio as precio', 'wishlists.id as wish_id')
        ->where('wishlists.id','=',$id)
        ->first();

        $precio = $articulo->precio;

        //dd( $monto);
    
        if($precio < $monto){
            $user_id = 1;
            $wishlist = Wishlist::where('id', $id)
                        ->update(['comprado' => 1, 'updated_at' => Carbon::now()]);
            $user = User::findOrFail($user_id);
        
            Mail::to($user->email)->send(new BuyProduct($articulo));
            return $this->successResponse(
                            'Articulo Comprado exitosamente',
                            202
                        );
        }else{
            return $this->errorResponse('No tiene sufficiente presupuesto', 404);  
        }
  
    }

    public function destroy($id)
    {
        $wishlist = Wishlist::findOrFail($id);
        $wishlist->delete();
    }
   
}
