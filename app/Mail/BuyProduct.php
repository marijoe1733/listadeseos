<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BuyProduct extends Mailable
{
    use Queueable, SerializesModels;
    public $articulo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($articulo)
    {
        $this->articulo = $articulo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.notificacionCompra');
    }
}
