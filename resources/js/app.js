
new Vue({
   el: '#crud',
   created: function() {
       this.getKeeps();
       
   },
   data: {
       keeps: [],
       articulo_id:'',
       showByIndex: null,
       errors: '',
       fillKeep: {'id': '', 'nombre':'', 'foto':'', 'descripcion':''},
   },
   methods: {
       getKeeps: function() {
           var urlKeeps = 'api/articulos';
           axios.get(urlKeeps).then(response => {
               this.keeps = response.data
           });
       }, 
       showKeep: function(keep) {
        this.fillKeep.id     = keep.id;
        this.fillKeep.nombre = keep.nombre;
        this.fillKeep.foto = keep.foto;
        this.fillKeep.descripcion = keep.descripcion;
        $('#mostrar').modal('show');
      },
       createWish: function() {
        var url = 'api/addTowishList';
        axios.post(url,{articulo_id :this.fillKeep.id}).then(response => {
           
            $('#mostrar').modal('hide');
            toastr.success('Articulo agregado a la lista de Deseo. Checa su correo');
        }).catch(error => {
            toastr.error('Este Articulo ya existe en la lista');
        });
 
   }
}
});





 
