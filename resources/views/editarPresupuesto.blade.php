<form method="POST" v-on:submit.prevent="editarPresupuesto()">
<div class="modal fade" id="edit">
	<div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>                  
             </div>
			<div class="modal-body">
				<label for="presupuesto">Cambiar Presupuesto</label>
				<input type="text" name="presupuesto" class="form-control" v-model="pre">
			</div>
			<div class="modal-footer">
				<input type="submit" class="btnGuardar" value="Cambiar">
			</div>
		</div>
	</div>
</div>
</form>