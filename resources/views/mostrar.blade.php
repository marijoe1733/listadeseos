<form method="POST" v-on:submit.prevent="createWish()">
<div class="modal fade" id="mostrar">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
                  <h3><b>@{{this.fillKeep.nombre}}</b></h3>
                  
             </div>
             <div class="modal-body" >
                <img :src="this.fillKeep.foto" style=" max-width:70%;" >
                <p>@{{this.fillKeep.descripcion}}</p>
             </div>
             <input type="hidden" name="articulo_id" class="form-control" v-model="articulo_id">
             <div class="modal-footer">
             <input type="submit" class="btnGuardar" value="Add to wishList">
            </div>
         </div>
     </div>
</div>


 </form>