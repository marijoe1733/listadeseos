@extends('app')

@section('content')
<div id ="crud" class="row">
    <div class="col-xs-12">
        <h1 class="page-header">Lista de Articulos</h1>
    </div>

      <div class="wrap-icon right-section col-md-12">
			<div class="wrap-icon-section wishlist">
        <a href="{{ url('wishlist') }}">
        	<i class="fa fa-heart" aria-hidden="true"  ></i>
					<div class="left-info">
				
					<span class="title">Wishlist</span>
					</div>
				</a>
		  </div>
        </div>
      <div class="row">
        <div v-for="keep in keeps" class="col-md-4" style="width:25%;"@mouseover="showByIndex = keep" @mouseout="showByIndex = null" >
            <div class="container">
                <img :src="keep.foto" class="list" style="width:100%; max-width:150px;" >

                <button class="btn" v-show="showByIndex === keep" v-on:click.prevent="showKeep(keep)">Quick view</button>
                </div>
                <h3><b> @{{keep.nombre}}</b></h3>
                <p> @{{keep.descripcion}}</p>
                <span><b>$@{{keep.precio}}</b></span>
            </div>
      </div>
      @include('mostrar')
    </div>

      @include('list')


@endsection