<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">

    <title>Lista de deseos</title>
    <style>
  .btnGuardar{
    background-color: #ff6666;
    color: white;
    border-radius: 20px;
    border-color: white;
    width: 150px;
    height: 40px;
  }

  .correo1{
    text-align: center;
  }
</style>
</head>

<body class="correo1">
    <p>Hola! Se ha agregado un articulo nuevo en tu lista de deseo.</p>
    <p>De click en el sigiuente boton </p>

<br>
    <a href="{{ url('wishlist') }}">
    <input type="submit" class="btnGuardar" value="Ir a la lista">
    </a>
</body>
</html>