<!doctype html>
<html lang="en">
  <head>
  	<title>Lista de deseo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


	</head>
	<body>
	<section class="ftco-section" id="wishList">
		<div class="container" >
		<div class="row justify-content-center">
			<div class="col-md-6 text-left mb-4">
				<a href="{{ url('/') }}">
			    	<i class="fa fa-home" aria-hidden="true"  >Menu</i>
			  </a>
			</div>
			<div class="col-md-6 text-right mb-4" >
			  <div class="row text-left" >
			    <div class="col-md-6">
					<span >Presupuesto Inicial: $ @{{pre}}</span>
					<button type="button" data-toggle="modal" data-target="#edit" class="btn btn-primary list" >Cambiar Presupuesto</button>
				</div>
				<div class="col-md-6">
					<span>Monto Disponible: $ @{{monto}}</span>
					<br>
					<span style="color: red">Monto gastado: $ @{{totalGastado}}</span>

				</div>
				
			</div>

			</div>
		</div>
		<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-4">
                <i class="fa fa-heart" aria-hidden="true"  ></i>
					<h2 class="heading-section">Lista de Deseos</h2>
				</div>
		</div>
		<div class="row justify-content-center">
				<div class="col-md-6 text-left mb-4"  v-on:click="comprado()">
				  <button type="button" class="btn btn-primary" style="background-color: peachpuff; border-color: beige;">Ver Articulos comprados</button>
               </div>
			   <div class="col-md-6 text-right mb-4"  v-on:click="Nocomprado()">
				  <button type="button" class="btn btn-primary" style="background-color: peachpuff; border-color: beige;" >Ver Articulos No comprados</button>
               </div>
		</div>	
			<div class="row" id="todos">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table">
						  <thead class="thead-primary">
						    <tr>
						    	<th>Comprado</th>
						    	<th>&nbsp;</th>
						    	<th>Descripcion</th>
                              <th>Nombre</th> 
						      <th>Precio</th>
						      <th>Fecha de publicacion</th>
						      <th>total</th>
						      <th>&nbsp;</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr v-for="articulo in articulos" :key="articulo.id" class="alert" role="alert">
							
						    	<td>
						    		<label class="checkbox-wrap checkbox-primary">
										  <input type="checkbox"  v-if ="articulo.status == 1" checked disabled="disabled"  >
										  <input type="checkbox"  v-else-if ="articulo.status == 0 " @click="updateArticulos(articulo)">
				
										  <span class="checkmark"></span>
										</label>
						    	</td>
						    	<td>
						    		<div class="img" style="background-image:url('@{{articulo.foto}}');"></div>
						    	</td>
						      <td>
						      	<div class="email">
						      		<span>@{{articulo.descripcion}}</span>
						      		<span></span>
						      	</div>
						      </td> 
                              <td>@{{articulo.nombre}}</td>
						      <td>@{{articulo.precio}}</td>
							  <td>@{{articulo.fecha}}</td>
				          </td>
						  
				          <td>@{{$total}}</td>
						      <td>
						      	<button type="button" class="close"  data-dismiss="alert" aria-label="Close">
				            	<span aria-hidden="true" v-on:click.prevent="deleteArticulo(articulo)"><i class="fa fa-close"></i></span>
				          	</button>
				        	</td>
						    </tr>
						
						  </tbody>
						</table>
					</div>
				</div>
			</div>


			<div class="row" id="todosComprado">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table">
						  <thead class="thead-primary">
						    <tr>
						    	<th>Comprado</th>
						    	<th>&nbsp;</th>
						    	<th>Descripcion</th>
                              <th>Nombre</th> 
						      <th>Precio</th>
						      <th>Fecha de publicacion</th>
						      <th>total</th>
						      <th>&nbsp;</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr v-for="articulo in articulosCom" :key="articulo.id" class="alert" role="alert">
							
						    	<td>
						    		<label class="checkbox-wrap checkbox-primary">
										  <input type="checkbox"  v-if ="articulo.status == 1" checked disabled="disabled" >
										  <span class="checkmark"></span>
										</label>
						    	</td>
						    	<td>
						    		<div class="img" style="background-image:url('@{{articulo.foto}}');"></div>
						    	</td>
						      <td>
						      	<div class="email">
						      		<span>@{{articulo.descripcion}}</span>
						      		<span></span>
						      	</div>
						      </td> 
                              <td>@{{articulo.nombre}}</td>
						      <td>@{{articulo.precio}}</td>
							  <td>@{{articulo.fecha}}</td>
				          </td>
						  
				          <td>@{{$total}}</td>
						     
						    </tr>
						
						  </tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="row" id="todosNoComprado">
				<div class="col-md-12">
					<div class="table-wrap">
						<table class="table">
						  <thead class="thead-primary">
						    <tr>
						    	<th>Comprado</th>
						    	<th>&nbsp;</th>
						    	<th>Descripcion</th>
                              <th>Nombre</th> 
						      <th>Precio</th>
						      <th>Fecha de publicacion</th>
						      <th>total</th>
						      <th>&nbsp;</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr v-for="articulo in articulosNoCom" :key="articulo.id" class="alert" role="alert">
							
						    	<td>
						    		<label class="checkbox-wrap checkbox-primary">
										  <input type="checkbox"  v-if ="articulo.status == 1" checked >
										  <input type="checkbox"  v-else-if ="articulo.status == 0" @click="updateArticulos(articulo)">
				
										  <span class="checkmark"></span>
										</label>
						    	</td>
						    	<td>
						    		<div class="img" style="background-image:url('@{{articulo.foto}}');"></div>
						    	</td>
						      <td>
						      	<div class="email">
						      		<span>@{{articulo.descripcion}}</span>
						      		<span></span>
						      	</div>
						      </td> 
                              <td>@{{articulo.nombre}}</td>
						      <td>@{{articulo.precio}}</td>
							  <td>@{{articulo.fecha}}</td>
				          </td>
						  
				          <td>@{{$total}}</td>
						     
						    </tr>
						
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	@include('editarPresupuesto')

	
	</section>
	

  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/popper.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/main.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<script>
new Vue({
    el: '#wishList',
    created: function() {
		this.showPresupuesto();
		this.showTotal();
        this.getArticulos();
		this.showMonto();
    },
    data: {
        articulos: [],
		articulosCom: [],
		articulosNoCom: [],
		pre: {'presupuesto': 0},
		totalGastado:{'total': 0},
		monto: {'monto':0},
		errors: '',




		
    },
    methods: {
        getArticulos: function() {
			$('#todosComprado').hide();
			$('#todosNoComprado').hide();
			var urlArticulos = 'api/wishList';
            axios.get(urlArticulos).then(response => {
                this.articulos = response.data
				console.log(this.articulos);
				
            });
        },
	
      deleteArticulo: function(articulo) {
        var url = 'api/deleteTowishList/' + articulo.id;
	      axios.delete(url).then(response => {
            this.getArticulos();            
        });
    },
	comprado: function() {
		$('#todos').hide();
		$('#todosNoComprado').hide();
		$('#todosComprado').show();
		var urlArticulos = 'api/wishList/comprado';
            axios.get(urlArticulos).then(response => {
                this.articulosCom = response.data
				
            });
        
    },
	Nocomprado: function() {
		  
		$('#todos').hide();
		$('#todosComprado').hide();
		$('#todosNoComprado').show();
		var urlArticulos = 'api/wishList/Nocomprado';
            axios.get(urlArticulos).then(response => {
                this.articulosNoCom = response.data
				
            });
			
		},
    updateArticulos: function(articulo){
		console.log(articulo);

		var url = 'api/updateWishlist/' + articulo.id;
	      axios.get(url).then(response => {
            this.getArticulos();  
			toastr.success('Articulo Comprado! Checa su correo');
			location.reload();  
        }).catch(error => {
            toastr.error('No tiene suficiente presupuesto');
			location.reload();  


        });
    },


	showPresupuesto: function(){
		var url = 'api/showPresupuesto';
            axios.get(url).then(response => {
				this.presupuesto = response.data
				this.pre = JSON.stringify(this.presupuesto[0].presupuesto);
				
            });
	},

	editarPresupuesto: function(){
		var url = 'api/cambiarPresupuesto/';
	      axios.post(url,{presupuesto :this.pre}).then(response => { 
			$('#edit').hide();
			toastr.success('Presupuesto Cambiado');
			location.reload();
          
        });
	},
	showTotal: function(){
		var url = 'api/total';
            axios.get(url).then(response => {
				this.totalGastado = response.data;
				
            });
	},
	showMonto: function(){
		var url = 'api/monto';
            axios.get(url).then(response => {
				this.monto = response.data

				console.log(this.monto);
				
            });
	},
	
}
});
</script>


	</body>
</html>

