<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticuloController;
use App\Http\Controllers\WishlistController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/articulos',[ArticuloController::class, 'index']);
Route::get('/articulo/{id}',[ArticuloController::class, 'obtenerArticulo']);
Route::get('/wishList',[WishlistController::class, 'index']);
Route::post('/addTowishList',[WishlistController::class, 'guardar']);
Route::delete('/deleteTowishList/{id}',[WishlistController::class, 'destroy']);
Route::get('/wishList/comprado',[WishlistController::class, 'comprado']);
Route::get('/wishList/Nocomprado',[WishlistController::class, 'Nocomprado']);
Route::get('/updateWishlist/{id}', [WishlistController::class, 'update']);
Route::get('/updateWishlist/{id}', [WishlistController::class, 'update']);
Route::get('/showPresupuesto', [WishlistController::class, 'showPresupuesto']);
Route::post('/cambiarPresupuesto', [WishlistController::class, 'cambiarPresupuesto']);
Route::get('/total', [WishlistController::class, 'precioTotal']);
Route::get('/monto', [WishlistController::class, 'montoDisponible']);














