<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Articulo;

use Carbon\Carbon;

class ArticuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Articulo::create([
            'nombre' => 'Ipad',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_01.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 1977,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Smartphone',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_02.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 200,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Devices',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_03.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 170,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Watch',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/fashion_04.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 500,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Shoes',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/fashion_05.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 137,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Laptop',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_04.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 500,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Tablette',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_05.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 137,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Headphone',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_13.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 777,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Lenovo',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_14.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 237,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Pants',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/fashion_03.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 77,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'T-shirt',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/fashion_07.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 2372,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Tocador',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/furniture_08.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 777,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Sillon',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/furniture_09.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 2372,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Sofa',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/furniture_05.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 197,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Cama',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/furniture_06.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. ',
            'precio' => 270,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Segway',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_15.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 197,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Bocina',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_16.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 270,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Tv',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/digital_17.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 977,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        Articulo::create([
            'nombre' => 'Lampara',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/furniture_07.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 270,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        Articulo::create([
            'nombre' => 'Corbata',
            'foto' => 'http://localhost:8000/laravel-vue-lista/public/images/fashion_06.jpg',
            'descripcion' => 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
            'precio' => 977,
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
